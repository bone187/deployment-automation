﻿Add-Type -AssemblyName System.Windows.Forms

#Checking if OS is x86 or x64
$SVNCheck = Get-WmiObject -Class Win32_OperatingSystem | Select-Object OSArchitecture
#Trying to find the right svn.exe
Switch ($SVNCheck -match "64-Bit" )
{
    true {$SVNEXE = "$env:ProgramFiles\TortoiseSVN\bin\svn.exe";break}
    false {$SVNEXE = "$env:ProgramFiles(x86)\TortoiseSVN\bin\svn.exe";break}
}

Function Save-SVNExport ($CSVImport)
{$SetExportLocation = New-Object System.Windows.Forms.FolderBrowserDialog
$SetExportLocation.Description = "Enter Path"
$SetExportLocation.SelectedPath = "C:\Temp\"
    If ($SetExportLocation.ShowDialog() -eq "Ok"){
    $p = $SetExportLocation.SelectedPath
    $SCRIPT:arg5 =$p}
    Else {Write-Host -BackgroundColor Red "Aborted!"; exit} 
    }

Function Import-InstallationHints ($CSVImport)
{$CSVImport | foreach {$URL=$_.Ressource; $SVNrevision = $_.Revision; $target=$_.Target;
$arg1 = "export"
$arg2 = "-r"
$arg3 = $SVNrevision
$arg4 = "`"" + $URL + "`"" # URL to Repository in Quotes eg "http://svn.example.com"
$TargetPath =  $SCRIPT:arg5 + "\" + $target
$arg6 = "--force"
$arg7 = "-q"
$testpath = Test-Path $TargetPath
If ($testpath -eq $false) {New-Item -Path $TargetPath -ItemType Diretory}
$SVNEXEReturnValues =  & $SVNEXE $arg1,$arg2,$arg3,$arg4,$TargetPath,$arg6,$arg7
Out-File -FilePath "$SCRIPT:arg5 + exportlog.txt" -Append -Width "160" -InputObject $SVNEXEReturnValues -Encoding utf8
}}

$GetCSVLocation = New-Object System.Windows.Forms.OpenFileDialog
$GetCSVLocation.Title = "Enter path to CSV File"
$GetCSVLocation.InitialDirectory = $env:USERPROFILE
$GetCSVLocation.Filter = "CSV | *.csv"
$GetCSVLocation.Multiselect = $false
If ($GetCSVLocation.ShowDialog() -eq "Ok")
    {$CSVImport = Import-Csv -Delimiter ";" -Path $GetCSVLocation.FileName}
Else
    {Write-Host -BackgroundColor Red "Aborted!"; exit}